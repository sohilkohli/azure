resource "azurerm_virtual_network" "testvm" {
  name                = "terranetwork"
  address_space       = ["10.1.0.0/16"]
  location            = var.location
  resource_group_name = azurerm_resource_group.testvm.name
}

resource "azurerm_subnet" "testvm" {
  name                 = "subnetA"
  resource_group_name  = azurerm_resource_group.testvm.name
  virtual_network_name = azurerm_virtual_network.testvm.name
  address_prefixes     = ["10.1.2.0/24"]
}

resource "azurerm_network_interface" "testvm" {
  name                = "testvm-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.testvm.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.testvm.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.publicip.id
  }
}

resource "azurerm_windows_virtual_machine" "testvm" {
  name                = "testvm-machine"
  resource_group_name = azurerm_resource_group.testvm.name
  location            = var.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  admin_password      = "Mani@28121989"
  network_interface_ids = [
    azurerm_network_interface.testvm.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
}

resource "azurerm_managed_disk" "example" {
  name                 = "data-disk1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.testvm.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "example" {
  managed_disk_id    = azurerm_managed_disk.example.id
  virtual_machine_id = azurerm_windows_virtual_machine.testvm.id
  lun                = "10"
  caching            = "ReadWrite"
}

resource "azurerm_public_ip" "publicip" {
  name                = "PublicIp1"
  resource_group_name = azurerm_resource_group.testvm.name
  location            = var.location
  allocation_method   = "Static"
}

resource "azurerm_network_security_group" "nsg" {
  name                = "First-nsg"
  location            = var.location
  resource_group_name = azurerm_resource_group.testvm.name

  security_rule {
    name                       = "Port80"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "nsgassociation" {
  subnet_id                 = azurerm_subnet.testvm.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}